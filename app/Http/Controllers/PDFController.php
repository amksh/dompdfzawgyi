<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PDFController extends Controller
{
    public function download()
    {   
        $data = ['name' => 'ကျော်ကျော်', 'address' => 'ရန်ကုန်'];
    	$mgmg = \Rabbit::uni2zg($data['name']);
    	$kk = \Rabbit::uni2zg($data['address']);
        $pdf = \PDF::loadView('pdf', compact('mgmg', 'kk'));
        return $pdf->download('dompdfsh.pdf');
    }
}
